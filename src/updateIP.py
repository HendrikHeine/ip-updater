import requests
import logging
from requests.auth import HTTPBasicAuth
from secretHandler import secret as Secret
from time import sleep

# Init Logger
logger = logging.getLogger('ipUpdater')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='data/log/ipUpdater.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s|%(levelname)s|%(name)s|:%(message)s'))
logger.addHandler(handler)


class IpUpdater:
    def __init__(self, intervalInSeconds=120) -> None:
        self.__intervalInSeconds = intervalInSeconds
        self.__secret = Secret()
        self.__url = self.__secret.loadSecret(fileName="secret.json", item="url")
        self.__user = self.__secret.loadSecret(fileName="secret.json", item="user")
        self.__passwd = self.__secret.loadSecret(fileName="secret.json", item="passwd")
        self.__checkSecrets()

    def __checkSecrets(self):
        if self.__url[0] == 0: self.__url = self.__url[1]
        else:exit(1)

        if self.__user[0] == 0: self.__user = self.__user[1]
        else:exit(1)

        if self.__passwd[0] == 0: self.__passwd = self.__passwd[1]
        else:exit(1)

    def __checkIP(self):
        response = requests.get("https://ipinfo.io/ip")
        return response.text

    def __updateIP(self, ip:str):
        request = f"{self.__url}{ip}"
        requests.put(url=request, auth=HTTPBasicAuth(self.__user, self.__passwd))

    def runLoop(self):
        ip = "0.0.0.0"
        while True:
            newIP = self.__checkIP()
            if newIP != ip:
                logger.info(f"IP changed: {ip} ---> {newIP}")
                self.__updateIP(newIP)
                ip = newIP
            sleep(self.__intervalInSeconds)