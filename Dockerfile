FROM python:3.11.2-alpine

WORKDIR /app

COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
RUN rm requirements.txt

COPY ./src/ .

RUN mkdir -p ./data/log

CMD [ "python3", "main.py" ]