# IP Updater

This is an IP Updater written for the Domain service [do.de](https://do.de). I'm not sure if this works for any other provider. The purpose of this software is to update the public IPv4 address for FlexDNS (like DynDNS).

## Getting started

U have to create a file called `secret.json` in the root folder. U can use the example file `secret.example.json`. I think the names r very self explaining...<br>
```json
{"url": "https://ddns.do.de/?myip=", "user": "DDNS-K00000-000000", "passwd": "PassWord"}
```

## Run
Run with pulling from [hub.docker.com](https://hub.docker.com/r/dasmoorhuhn/ip-updater)
```bash
docker-compose up -d
```
Run and build image from source code
```bash
docker-compose up --build -d
```
